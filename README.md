# Conception Objet Avancée -- Documents

This repository contains all documents for the cours COA - AA 2022-2023. 

## Slides

1. [Introduction - Classes - References ](01.introduction/01.introduction.pdf)
2. [STL - Inheritance - Virtual functions](02.inheritance/02.inheritance.pdf)
3. [Dynamic casting - static - const](03.static/03.static.pdf)
4. [Templates and exceptions](04.templates/04.templates.pdf)
5. [Memory management](05.memory/05.memory.pdf)
6. [Metaprogramming](06.metaprogramming/06.metaprogramming.pdf)


List of questions to prepare the exam:

- [Questions](questions.pdf)
