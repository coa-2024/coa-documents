#include <vector>
#include <list>
#include <iostream>

template <class C>
auto get5th(const C& container) 
{
    auto i = container.begin() + 5;
    return *i;
}

int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << get5th(v) << std::endl;

    std::vector<double> l = {1.0, 2.0, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << get5th(l) << std::endl;
}
