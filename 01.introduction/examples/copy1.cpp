#include "myclass.hpp"

//using namespace std;

void g(MyClass c) {
    c.fun(5);
}

void h(MyClass &c) {
    c.fun(5);
    //c.get();
} 

int main() {
    MyClass obj(2);

    std::cout << "Before calling g: obj.get() = " << obj.get() << std::endl;
    g(obj);
    std::cout << "After calling g: obj.get() = " << obj.get() << std::endl;
    h(obj);
    std::cout << "After calling h: obj.get() = " << obj.get() << std::endl;    
}
